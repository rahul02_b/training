
import modifier.*;

public class Account extends Bank {
    
    public String accountNumber;
    public String type;
    public int name;
    
    // parameterised constructor
    public Account(int accountNumber, String type, String name){
        this.accountNumber = accountNumber;
        this.type = type;
        this.name = name;
    }
    
    private Account(int balance, int accountNumber){
        System.out.println("your balance is : "+balance+"\naccountNumber :"+accountNumber+"\n");
    }
    
    // Non-Parameterized Constructor
    public Account(){
        System.out.println("account type :"+type+"\n"); 
    }
    // Method
    protected void printaccountDetails(){
        System.out.println("\naccount number : "+accountNumber+"\naccount type :"+type+"\nName:"+name+"\n");
    }
    
    {
        name = "Rahul";
    }
    
    public static void main(String args[]) {
        Account account = new Account("0925313912231", "savings", "Deepak");
        account.printaccountDetails();
        Account account1 = new Account("100000", "0925313912231");
        account1.printaccountDetails();
        Account account2 = new Account();
        Interest interest = new Interest();
        interest.roi();        			 // compile time error 
        Bank bank = new Bank();
        bank.details();                  // compile time error
        bank.details();
    }
     
  
}
