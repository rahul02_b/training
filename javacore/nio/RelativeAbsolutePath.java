/*
Question :
	 Write a program to demonstrate absolute and relative path.

WBS :

1.Requirements:
      Program to demonstrate absolute and relative path.
    
2.Entities:
      RelativeAbsolutePath
    
  
4.Jobs to be done:
    1.Store the path to create a file specified path.
    2.Create FileOutputStream class with path and true value to create file.
    3.Create PrintWriter class to write a file 
          3.1)Write in file using println 
          3.2)Close the abosluteprinter.
    4.Create FileOutputStream class with file name and true value to create file.
    5.Create PrintWriter class to write a file 
          5.1)Write in file using println 
          5.2)Close the abosluteprinter.
    
Pseudo Code:

public class RelativeAbsolutePath {
	public static void main(String[] args) throws IOException {

		String aboslute = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio\\absoulte.txt";
		FileOutputStream aboslutefos = new FileOutputStream(aboslute, true);
		PrintWriter abosluteprinter = new PrintWriter(aboslutefos);
		abosluteprinter.println("Sample File For Absolute");
		abosluteprinter.close();
		
		FileOutputStream fos = new FileOutputStream("relative.txt", true);
		PrintWriter printer = new PrintWriter(fos);
		
		printer.println("Sample File For Relative");
		printer.close();
		System.out.println("File Successfully Created and Written");
	}
}

*/

package com.kpriet.java.nio;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class RelativeAbsolutePath {
	public static void main(String[] args) throws IOException {

		String aboslute = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio\\absoulte.txt";
		FileOutputStream aboslutefos = new FileOutputStream(aboslute, true);
		PrintWriter abosluteprinter = new PrintWriter(aboslutefos);
		abosluteprinter.println("Sample File For Absolute");
		abosluteprinter.close();
		
		FileOutputStream fos = new FileOutputStream("relative.txt", true);
		PrintWriter printer = new PrintWriter(fos);
		
		printer.println("Sample File For Relative");
		printer.close();
		System.out.println("File Successfully Created and Written");
	}
}
