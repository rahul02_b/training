/*
Question :
	2) Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).
 
WBS :

1.Requirements:
      Program to  Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).
    
2.Entities:
      FileSearch
  
4.Jobs to be done:
    1.Create file SimpleFileVisitor class with generic type of Path 
    2.Invoke the walkFileTree method it invoke
       2.1)postVisitDirectory method to return enum SKIP_SUBTREE and SKIP_SIBLINGS.
       2.2)visitFile method to return the directory containing all file paths
       2.3)Invoke visitFile method to check exists file or not.
       
Pseudo Code:

public class FileSearch {

	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@SuppressWarnings("static-access")
			public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
				return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
		
			}
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				System.out.println(file);
				return super.visitFile(file, attrs);
			}
		};
		Path path = Paths.get();
		Files.walkFileTree(path, fv);
	}
}

*/

package com.kpriet.java.nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class FileSearch {

	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@SuppressWarnings("static-access")
			public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
				return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
		
			}
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				System.out.println(file);
				return super.visitFile(file, attrs);
			}
		};
		Path path = Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio");
		Files.walkFileTree(path, fv);
	}
}
