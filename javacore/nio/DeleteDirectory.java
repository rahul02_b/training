 /*  
Question :
	4) Delete the directory along with the files recursively.

1.Requirements:
      Program to delete the directory along with the files recursively.
    
2.Entities:
     DeleteVisitFileMethod
    
3.Methodsignature:
     public void deleteDir(File file)
4.Jobs to be done:
    1.Store files path directory to delete all files recursively.
    2.Create File class pass files path directory as argument.
    3.Check the file not exixts 
        3.1)Not exists print file not exist.
        3.2)Create an object for DeleteDirectory class deleteDir method.
    4.Check directory contains files and check length of files delete files using for each
       4.1)Files not in directory delete directory using delete method
    
Pseudo Code:

public class DeleteDirectory {
	private static final String src_delete = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio\\src_delete";
	public static void main(String[] args) throws IOException {
		File file = new File(src_delete);
		if (!file.exists()) {
			System.out.println("Directory does not exist.");
		} else {
			DeleteDirectory deleteDirectory = new DeleteDirectory();
			deleteDirectory.deleteDir(file);
		}
	}
	public void deleteDir(File file) throws IOException {
		if (file.isDirectory()) {
			if (file.list().length == 0) {
				deleteEmptyDir(file);
			} else {
				File files[] = file.listFiles();
				for (File fileDelete : files) {
					deleteDir(fileDelete);
				}
				if (file.list().length == 0) {
					deleteEmptyDir(file);
				}
			}
		} else {
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}

	private void deleteEmptyDir(File file) {
		file.delete();
		System.out.println("Directory is deleted : " + file.getAbsolutePath());
	}
}


*/

package com.kpriet.java.nio;

import java.io.File;
import java.io.IOException;

public class DeleteDirectory {

	private static final String src_delete = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio\\src_delete";

	public static void main(String[] args) throws IOException {
		File file = new File(src_delete);
		// make sure directory exists

		if (!file.exists()) {
			System.out.println("Directory does not exist.");
		} else {
			DeleteDirectory deleteDirectory = new DeleteDirectory();
			deleteDirectory.deleteDir(file);
		}

	}

	public void deleteDir(File file) throws IOException {

		if (file.isDirectory()) {

			// If directory is empty, then delete it

			if (file.list().length == 0) {
				deleteEmptyDir(file);
			} else {
				// list all the directory contents
				File files[] = file.listFiles();

				for (File fileDelete : files) {

					// Recursive delete

					deleteDir(fileDelete);
				}

				// check the directory again, if empty then
				// delete it.

				if (file.list().length == 0) {
					deleteEmptyDir(file);
				}
			}
		} else {
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}

	private void deleteEmptyDir(File file) {
		file.delete();
		System.out.println("Directory is deleted : " + file.getAbsolutePath());
	}
}
