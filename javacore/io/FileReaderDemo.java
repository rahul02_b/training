/*
Question :
	2. Reading a file using Reader

WBS

1.Requirement:
     Read a file using Reader

2.Entity:
     ReaderDemo

3.Jobs to be done:

    1.Get the file using file path and store file in source String.
      1.1)Check file exist using File class.
    2.Read the file with Reader type for FileReader.
    3.Create a character array of file length.
    4.Read the file of size of char array store to char array.
    5.Display the file content and close the reader.

Pseudo Code:
	
	public class ReaderDemo {
	    public static void main(String[] args) throws IOException {
	        File file = new File(source);
	        Reader reader = new FileReader(file);
	        char[] chars = new char[file.length()];
	        reader.read(chars);
	        System.out.println(chars);
	    }
	
	}

*/

package com.kpriet.java.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class FileReaderDemo {

    public static void main(String[] args) throws IOException {

        // source file location
        String source = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\io\\Content.txt";

        // getting the file
        File file = new File(source);

        // Reader to read the data present in file
        Reader reader = new FileReader(file);
        
        // creating a char array of size of file
        char[] character = new char[(int) file.length()];

        // read() gets the byes of data upto the size of char array
        // and stores the readed data to char array
        reader.read(character);
        System.out.println(new String(character));
        reader.close();
    }

}