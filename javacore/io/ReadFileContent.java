/*
Question :
	3. Read a any text file using BufferedReader and print the content of the file

WBS 

1.Requirement:
     Read a any text file using BufferedReader and print the content of the file

2.Entity:
     ReadFileContent

3.Jobs to be done:
     1.Create a file to write content.
     2.Write content to the file created using write method.
     3.Close the file after written using close method.
     4.Using FileReader to read the content of file.
     5.Buffered reader takes the Reader file.
     6.Read each line
         6.1)If a line exists on file show the line of data.
         6.2)If no more line exists, stop reading the file.
     7.Close the BufferedReader properly using close method.

Pseudo Code:

	public class ReadFileContent {
	    public static void main(String[] args) throws IOException {
	        FileWriter file = new FileWriter(source);
	        file.write("Hello");
        	file.write("\nThis I|O exception example ");
        	file.close();
	        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
	        String line;
	        while ((line = bufferedReader.readLine()) != null) {
	            System.out.println(line);
	        }
	        bufferedReader.close();
	    }
	}

*/

package com.kpriet.java.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadFileContent {

    public static void main(String[] args) throws IOException {
        // Create a file on specified location
        String source = "C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\io\\Content.txt";
        FileWriter file = new FileWriter(source);
        
        // Writing data to created file and closing the file
        file.write("Hello");
        file.write("\nThis I|O exception example ");
        file.close();

        // reader to read the data in file
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        // readLine reads data line by line
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        // closing the reader
        bufferedReader.close();
    }

}