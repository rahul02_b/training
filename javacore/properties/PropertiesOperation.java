/*
 Requirement:
       Write a program to perform the following operations in Properties.
         i) add some elements to the properties file.
         ii) print all the elements in the properties file using iterator.
         iii) print all the elements in the properties file using list method.
      
 Entity:
        PropertiesDemo
    
 Function Signature:
        public static void main(String[] args)
    
 Jobs to be done:
        1) Create a object for Property named property. 
        2) Add elements to the property.
        3) Create an iterator for the property.
        4) For each element in the property.
            4.1) Store the key in key.
            4.2) Store the value in value.
            4.3) Prin the key and value.    
        5) Print the element of the property using list method.
        
 Pseudo code:
  
 public class PropertiesDemo {

    public static void main(String[] args) throws IOException{
        
    	Properties properties = new Properties();
        
        // add the elements to the property
        
       
        Iterator<Object> iterator = properties.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String value = properties.getProperty(key);
            System.out.println("key : " + key + " , value : " + value);
        }
        properties.list(System.out);
    }
}
*/
package com.kpriet.java.properties;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

public class PropertiesOperation {

    public static void main(String[] args) throws IOException{
        
    	Properties properties = new Properties();
        properties.setProperty("1", "One");
		properties.setProperty("2", "Two");
		properties.setProperty("3", "Three");
		properties.setProperty("4", "Four");

        System.out.println("Print all the elements in the property file using iterator");
        Iterator<Object> iterator = properties.keySet().iterator();
        while (iterator.hasNext()) {
            String key =  (String)iterator.next();
            String value = properties.getProperty(key);
            System.out.println("key : " + key + " , value : " + value);
        }
        properties.list(System.out);
    }
}
