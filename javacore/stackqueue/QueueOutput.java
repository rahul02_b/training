/* 
Question :
  Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.

1.Requirements :
  	what will be output and complete the code.
  	
2.Entities:
  	QueueOutput 
  	
4.Job to be done :  
   1.Create a class name QueueOutput and declare main method
   2.In the main creating the Queue with generic String type 
      2.1 adding values using add() method.
   3.To print the peek values poll() method to get values and using peek() printing the peek value.
   
 Pseudo Code :
 
 public class QueueOutput{
    public static void main(String [] args) {
        Queue<String> bikes = new PriorityQueue<String>();
        bikes.add("Honda");
        bikes.poll();
        bikes.peek();
	}
 }
 
*/
package com.kpriet.java.stackqueue;

import java.util.PriorityQueue;
import java.util.Queue;
public class QueueOutput{
    public static void main(String [] args) {
        Queue<String> bikes = new PriorityQueue<String>();
        bikes.add("Honda");
        bikes.add("Duke");
        bikes.add("Royal Enfield");
        System.out.println(bikes);
        bikes.poll();
        System.out.println("After Using poll() method : "+bikes);
        System.out.println("Using peek() method : 1"+bikes.peek());  
    }
}