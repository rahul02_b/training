/* Create a queue using generic type and in both implementation Priority Queue,
   Linked list and complete following  
     add atleast 5 elements
     remove the front element
     search a element in stack using contains key word and print boolean value value
     print the size of queue
     print the elements using Stream 

1.Requirements :
    Create a stack using generic type and implement
     Push atleast 5 elements
     Pop the peek element
     search a element in stack and print index value
     print the size of stack
     print the elements using Stream

2.Entities:
    QueueDemo
     
3.Method Signature:
    linkedList()
    priority

4.Job to be done :  
   1.Create a class name QueueDemo and declare main method
   2.In the main creating object for class and invoking the two methods.
   3.In the linkedList() method creating Queue with generic String type with initailse Linkedlist and creating stream for displaying the Stack elements.
   		3.1 Adding 5 values to Queue using add() method 
 		3.2 using remove() method removing value specified value.
   		3.3 Search the specified value using search() method 
 		3.4 find size of stack using size() method.
   		3.5 Using contains() method finding the given value is in the Queue or not 
		3.6 using size() method finding size of queue.
   4.Printing the queue values using stream and for each value.
   8.In the priority() method creating queue with initailse PriorityQueue and also creating stream to print the queue values.
   9.Adding the values to queue using add() method and finding specified value using contains() method.
   10.Finding the size using size() method and printing the PriorityQueue values using stream and for each
   
Pseudo Code:

public class QueueDemo{
	
	public void linkedList() {
		 Queue<String> mobile = new LinkedList<String>();
	        Stream<String> stream = mobile.stream();
	        add() element to mobile
	        mobile.remove();
	        mobile.contains("Realme");
	        mobile.size();
	        stream.forEach((element) -> {
	           System.out.print(element+" ");  
	        });
	}
	public void priority() {
		 Queue<String> mobile = new PriorityQueue<String>();
	        Stream<String> stream = mobile.stream();
	        add() element to mobile
	        mobile.remove();
	        mobile.contains("Realme");
	        mobile.size();
	        stream.forEach((element) -> {
	           System.out.print(element+" ");  
	        });
	}
	 public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	queue.linkedList();
    	queue.priority();
    }
}
	
	        
*/
package com.kpriet.java.stackqueue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo{
	
	public void linkedList() {
		 Queue<String> mobile = new LinkedList<String>();
	        Stream<String> stream = mobile.stream();
	        mobile.add("Redmi");
	        mobile.add("Apple");
	        mobile.add("Realme");
	        mobile.add("Oppo");
	        mobile.add("Vivo");
	        
	        mobile.remove();
	        
	        System.out.println("Realme mobile is present : "+mobile.contains("Realme"));
	        
	        System.out.println("Size : "+mobile.size());  
	        
	        stream.forEach((element) -> {
	           System.out.print(element+" ");  
	        }); 
	}
	public void priority() {
		 Queue<String> mobile = new PriorityQueue<String>();
	        Stream<String> stream = mobile.stream();
	        mobile.add("Apple");        
	        mobile.add("Redmi");
	        mobile.add("Vivo");
	        mobile.add("Honor");
	        mobile.add("One plus");
	        
	        mobile.remove();
	        
	        System.out.println("Realme mobile is present : "+mobile.contains("Realme"));
	        
	        System.out.println("Size : "+mobile.size());   
	        
	        stream.forEach((element) -> {
	           System.out.print(element+" "); 
	        });   
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("\t Linked List ");
    	queue.linkedList();
    	System.out.print("\n\t Priority \n");
    	queue.priority();
    }
}