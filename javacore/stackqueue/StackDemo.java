/*  
 Question :
   Create a stack using generic type and implement
     Push atleast 5 elements
     Pop the peek element
     search a element in stack and print index value
     print the size of stack
     print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

1.Requirements :
    Create a stack using generic type and implement
      Push atleast 5 elements
      Pop the peek element
      search a element in stack and print index value
      print the size of stack
      print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.
  
2.Entities:
    StackDemo 

4.Job to be done :  
   1.Create a class name StackDemo and declare main method
   2.In the main creating the Stack with generic String type and creating stream for displaying the Stack elements.
   3.Pushing 5 values to stack using push() method and using pop() method displaying top value in stack.
   4.Search the specified value using search() method and find size of stack using size() method.
   5.Printing the stack values using stream and for each.
   6.To reverse a stack creating another stack removing the values using remove() method and pushing to new created 
     stack using push() method 
   7.Removing elements from stack adding to list using add() method.
   
Pseudo Code :

	public class StackDemo {	
	    public static void main(String [] args) {
	        Stack<String> languages = new Stack<String>();  
	        Stream<String> stream = languages.stream();
	        languages.push("HTML");        
	        languages.pop();
	        languages.size(); 
	        languages.search("SQL");
	        stream.forEach((element) -> {
	           System.out.print(element+" "); 
	        });    
	        Stack<String> stack = new Stack<String>(); 
	        while(languages.size() > 0) {
	                  stack.push(languages.remove(0)); 
	        }
	        while(stack.size() > 0){
	                languages.add(stack.pop());
	        } 
		}
	}   
 
*/

package com.kpriet.java.stackqueue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {
	
    public static void main(String [] args) {
        Stack<String> languages = new Stack<String>();  
        Stream<String> stream = languages.stream();
        languages.push("JavaScript");
        languages.push("Python");
        languages.push("Java");
        languages.push("SQL"); 
        languages.push("HTML");        
        languages.pop();  
        
        int size = languages.size();        
        System.out.println("Size : "+size);
        
        int index = languages.search("SQL");
        System.out.println("Search Index Value : "+ index);
        
        System.out.print("\nDisplay elements : "); 
        stream.forEach((element) -> {
           System.out.print(element+" "); 
        });    
        
        Stack<String> stack = new Stack<String>();
        System.out.println("\nDisplay elements in Stack : " + languages);
                
        System.out.print("\nReverse LIST : ");
        while(languages.size() > 0) {
                  stack.push(languages.remove(0)); 
        }

        while(stack.size() > 0){
                languages.add(stack.pop());
        } 
        System.out.print(languages);
    }
}