/*
Question :
	10.Iterate the roster list in Persons class and and print the person without using forLoop/Stream 

WBS

1.Requirement:
	Iterate the roster list in Persons class and and print the person without using forLoop/Stream 

2.Entity:
	PrintPersonIterator

4.Jobs to be done:	
	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. using iterator to print person details

Pseudo Code:

public class PrintPersonIterator {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.

        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }
        // iterator to iterate objects in sequence
        //calling printPerson method
	}
}

*/

package com.kpriet.java.streams;

import java.util.Iterator;
import java.util.List;

public class PrintPersonIterator {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        Iterator<Person> iterator = personList.iterator();
        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }
    }
}
