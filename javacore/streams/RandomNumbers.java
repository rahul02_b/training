/*
Question :
	List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API

WBS 

 1.Requirement:
	List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API
  
 2.Entity: 
 	 RandomNumbers
 
 3.Jobs To be Done: 
 	1. Create a list of type person and add the list values to it. 
 	2. Find the sum of all numbers in the list.
 	3. Find the max and min element in the list.
 	4. Display the result.
 	
Pseudo Code :

	public class RandomNumbers {
	
		public static void main(String[] args) {
		
			Integer array[] = new Integer[] {1, 6, 10, 25, 78}; 
			
			List<Integer> randomNumbers = Arrays.asList(array);
			int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
			
			// Find the max and min element in the list using max() and min()
		}
	}

*/
package com.kpriet.java.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class RandomNumbers {

	public static void main(String[] args) {
	
		Integer array[] = new Integer[] {1, 6, 10, 25, 78}; 
		
		List<Integer> randomNumbers = Arrays.asList(array);
		int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
		System.out.println("Sum of Number :"+sum);
		
		int max = IntStream.range(0, array.length).map(i -> array[i]).max().getAsInt();
		int min = IntStream.range(0, array.length).map(i -> array[i]).min().getAsInt();
		
		System.out.println("Maximum number:" +max);
		System.out.println("Mininum number: "+min);
	}
}
