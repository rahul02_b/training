/*
Question :
	1.write a program for java regex quantifer?

	  Quantifiers  :-
	    X*        Zero or more occurrences of X
		X?        Zero or One occurrences of X
		X+        One or More occurrences of X
		X{n}      Exactly n occurrences of X 
		X{n, }    At-least n occurrences of X
		X{n, m}   Count of occurrences of X is from n to m
	  
WBS :

1.Requirement :
	Write a program for Java String Regex quantifer.

2.Entities :
	Quantifier
	
3.Job to be Done :
	1.Create class Quantifier and main method.
	2.Create string sentence.
	3.Using pattern class and enter the quantifier patter in compile method.
	4.Using matcher class, check wheather pattern class matches the sentance.
	5.And the using while loop find matches found print the character and index.
	
Pseudo Code:
	public class Quantifier {
	     
	    public static void main(String[] args) { 
	    
	        String sentence = "abaabbaaabbbaaaabab";
	        Pattern pattern = Pattern.compile("a*");
	        Matcher matcher = pattern.matcher(sentence);   
	        while(matcher.find()){
	            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
	        }    
	    }
	}

*/

package com.kpriet.java.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class Quantifier {
     
    public static void main(String[] args) { 
    
        String sentence = "abaabbaaabbbaaaabab";
        Pattern pattern = Pattern.compile("a*");
        Matcher matcher = pattern.matcher(sentence);   
        while(matcher.find()){
            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
        }    
    }
}
