/*
Question :

2. For the following code use the split method() and print in sentance
    String website = "https-www-google-com";
    
WBS :

1.Requirement :
	Write a program for Java String Regex Methods.

2.Entities :
	SplitMethod
	
3.Job to be Done :
	1.Create class SplitMethod and main method.
	2.Using string website and use split() method, splits the string into N substrings 
	  and returns a sentance.
	3.Print sentance using for each loop.
	
Pseudo Code:
	public class SplitMethod {
		public static void main(String[] args) {
			String website = "https-www-google-com";
		    // using the split method() to split text and print it  
		}
	}

*/

package com.kpriet.java.regex;

public class SplitMethod {
	
	public static void main(String[] args) {
		String website = "https-www-google-com";
	    for (String display: website.split("-")) {
	         System.out.print(display+" ");
	    }  
	}
}
