/*
Question :-

1.Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
	The discount rate is 10% for the quantity purchased between 100 and 120 units, 
	and 15% for the quantity purchased greater than 120 units. 
	If the quantity purchased is less than 100 units, the discount rate is 0%. 
	See the example output as shown below:
	Enter unit price: 25
	Enter quantity: 110
	The revenue from sale: 2475.0$
	After discount: 275.0$(10.0%)
		  
WBS :

1.Requirement :
	Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
    The discount rate is 10% for the quantity purchased between 100 and 120 units, 
	and 15% for the quantity purchased greater than 120 units. 
	If the quantity purchased is less than 100 units, the discount rate is 0%. 
	See the example output as shown below:
	Enter unit price: 25
	Enter quantity: 110
	The revenue from sale: 2475.0$
	After discount: 275.0$(10.0%)
	
2.Entities :
	LogicalOperator
	
3.Method Signature :
    calculateSale()
	
3.Job to be Done :
	1.Create class LogicalOperator and main method.
	2.Create calculateSale() method 
	3.And calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
	4.And then print the unit price, quantity, revenue and discount. 
	
Pseudo Code :
	public class LogicalOperator {
		public static void main(String[] args) {
			calculateSale();
		}
		public static void calculateSale() {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Enter unit price:");
			unitprice=scanner.nextFloat();
			System.out.print("Enter quantity:");
			quantity=scanner.nextInt();
		//  And calculate the revenue from a sale based on the unit price 
		    and quantity of a product input by the user	
		// Display the unit price, quantity, revenue and discount.
		}
	}
*/

package com.kpriet.java.regex;

import java.util.*;

public class LogicalOperator {
	public static void main(String[] args) {
		calculateSale();
	}

	public static void calculateSale() {

		float unitprice=0f;
		int quantity=0;
		float revenue=0f;
		float discount_rate=0f, discount_amount=0f;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter unit price:");
		unitprice=scanner.nextFloat();
		System.out.print("Enter quantity:");
		quantity=scanner.nextInt();

		if(quantity<100)
			revenue = unitprice*quantity;
		else if(quantity>=100 && quantity<=120)
		{
			discount_rate = (float)10/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		else if(quantity>120) {
			discount_rate = (float)15/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		System.out.println("The revenue from sale:"+revenue+"$");
		System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");
		}
}
