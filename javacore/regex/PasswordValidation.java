/* 
Question :- 
   1. create a pattern for password which contains
	   8 to 15 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit
WBS :

1.Requirement :
	 Create a pattern for password which contains
	   8 to 15 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit

2.Entities :
	PasswordValidation
	
3.Job to be Done :
	1.Create class PasswordValidation and main method.
	2.Create string password.
	3.Using pattern class and enter the condition for password in compile method.
	4.Using matcher class, check wheather pattern class matches the password.
	5.If matcher find print it is valid username and else not valid.
	
Pseudo Code:
	public class PasswordValidation {
		 
	    public static void main(String[] args) {
	        
	        String password = "PassWord12";
	        // create pattern for password
	        Matcher m = p.matcher(password);   
	        if(m.find()){
	            System.out.println(password + " is valid Password");
	        }
	        else {
	            System.out.println(password + " is not valid Password");
	        }
	    }
	}

*/


package com.kpriet.java.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidation {
	 
    public static void main(String[] args) {
        
        String password = "PassWord12";
        Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,15}");
        Matcher m = p.matcher(password);   
        if(m.find()){
            System.out.println(password + " is valid Password");
        }
        else {
            System.out.println(password + " is not valid Password");
        }
    }
}
