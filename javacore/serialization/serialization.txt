Serialization

1) What is serialization?

	Serialization is a mechanism for storing an object�s states into a persistent storage like disk files, 
databases, or sending object�s states over the network.


2) What is need of serialization?

    * It is easy to use and can be customized.
    * The serialized stream can be encrypted, authenticated and compressed, supporting the needs of secure Java computing.
    * Serialized classes can support coherent versioning and are flexible enough to allow gradual evolution of your application's
    object schema.


3) What will happen if one of the member in the class doesn�t implement serializable interface?
	
	When you try to serialize an object which implements Serializable interface, incase if the object includes 
a reference of an non serializable object then NotSerializableException will be thrown.

 