/*  Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
	+ Explain about contains(), subList(), retainAll() and give example
 
1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - ListDemo
3.Method Declaration

4.Jobs to be done
   1.Create a class and declaring main method.
   2.Inside the main creating list called topics and adding 10 topics using add() method and printing the added topics list.
   3.Creating another list of newTopics and adding 4 newTopics using addAll() method adding topics and newCars topics.
   4.Finding the index of given topic using indexOf() and lastIndexOf().
   5.Print the values in the list using
             1.In For loop getting index value and printing the value
             2.In Foreach printing the value assigning to variable topic
             3.In While loop with hasNext printing the value.
             4.Stream API using :: method reference printing the value.
   6.Converting the list into array and set.
   7.Checking the set with contain() method to set containing or not.
   8.Creating new list adding values from already created list topics using sublist(stratIndex, lastIndex) method.
   9.Finding the added values newly created list and topics list using retainAll() method.

Pesudo Code:
	public class ListDemo {
		public static void main(String[] args) {
			List<String> topics = new ArrayList<String>();
			// add the value to the list
			// create another list and perform addAll() method with it
			// find the index of some value with indexOf() and lastIndexOf()
			// print the values in the list using, For loop, For Each, Iterator, Stream API
			// convert the list to a set
			// convert the list to a array
			// using the method contains(), subList(), retainAll()
			// display all the result
		}
	}
	


*/

package com.kpriet.java.list;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {
	public static void main(String[] args) {
		
	List<String> topics = new ArrayList<String>();
	topics.add("Apple");
	topics.add("Samsung");
	topics.add("One plus");
	topics.add("Redmi");
	topics.add("Realme");
	topics.add("Vivo");
	topics.add("Oppo");
	topics.add("Lava");
	topics.add("Moto");
	topics.add("Poco");
	
	System.out.println("Top Brand Mobiles:\n"+topics);
	
	//Create another list and perform addAll() method with it
	List<String> newTopics = new ArrayList<String>();
	newTopics.add("Honor");
	newTopics.add("Realme");
	newTopics.add("Tecno");
	topics.addAll(newTopics);
	System.out.println("Top More Mobile Brands:\n"+topics);
	
	//Find the index of some value with indexOf() and lastIndexOf()
	System.out.println("Index of JavaCoreTopic Classes \t" + topics.indexOf("Oppo"));
	System.out.println("Last index of JavaCoreTopic Variables\t" + topics.lastIndexOf("Realme"));
	    
	//Print the values in the list using 
	System.out.println("\n Print the values in list Using For loop \n");
    for (int index = 0; index < topics.size(); index++) {
        System.out.println(topics.get(index));
    }

    //Using For each
    System.out.println("\n Print the values in list Using For each \n");
    for (String topic : topics) {
        System.out.println(topic);
    }

    //Using While loop
    System.out.println("\n Print the values in list Using While loop \n");
    Iterator<String> topicsIterator = topics.iterator();
    while(topicsIterator.hasNext()) {
        System.out.println(topicsIterator.next());
    }

    //Using Stream API's forEach
    System.out.println("\"\\n Print the values in list Using Strem Api \\n\"");
    topics.forEach(System.out::println);

    //Convert the list to a set
    Set<String> topicsSet = new HashSet<>(topics);
    System.out.println("SET : "+ topicsSet);

    //Convert the list to a array
    String[] topicsArray = new String[topics.size()];
    topicsArray = topics.toArray(topicsArray);
    System.out.print("LIST : ");
    for (String s : topicsArray)
        System.out.print(s+" ");
    System.out.println();
    //contains()
    System.out.print("contains() : ");
    System.out.println(topics.contains("Realme"));

    //subList(startIndex, lastIndex)
    List<String> newAddedTopics = topics.subList(10, topics.size());
    System.out.println("SUBLIST : "+newAddedTopics);

    //retainAll()
    topics.retainAll(newAddedTopics) ;
    System.out.println("After using retainAll method in topics list: \n" + topics);
	}
}
