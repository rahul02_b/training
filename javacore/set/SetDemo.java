/*
+ Create a set
    => Add 10 values
    => Perform addAll() and removeAll() to the set.
    => Iterate the set using 
        - Iterator
        - for-each
+ Explain the working of contains(), isEmpty() and give example.

1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - SetDemo
3.Method Declaration

4.Jobs to be done
   1.Create a class and declaring main method.
   2.Using in-build function add() method to insert 10 values.
   3.Using in-build function perform addAll() and removeAll() method.
   4.Display the set using while loop with hasNext() and forEach.
   5.Checking the set with contain() method to set containing or not and to check set is empty or not using isEmpty() method.

Pseudo Code:

	public class SetDemo{
		public static void main(String[] args) {
			Set<String> apps = new HashSet<>();
			add() - add 10 value to apps 
			Set<String> newapp = new HashSet<>();
			add() - add few more value to newapp
			apps.addAll(newapp);
			apps.removeAll(newapp);
			apps.contains("Facebook");
			newapp.isEmpty();
			Iterator<String> app = apps.iterator();
        	while (app.hasNext()) {
            	System.out.println(app.next());
        	}
		}		
	}
	
*/

package com.kpriet.java.set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo{

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> apps = new HashSet<>();
        apps.add("Youtube");
        apps.add("Amazon");
        apps.add("Flipkart");
        apps.add("Instagram");
        apps.add("Facebook");
        apps.add("MX Player");
        apps.add("Chrome");
        apps.add("Drive");
        apps.add("Gmail");
        apps.add("Maps");
        System.out.println("SET : " + apps +"\n");
        
        //Creating another set with some elements.
        Set<String> newapp = new HashSet<>();
        newapp.add("Linkedin");
        newapp.add("Phone pe");
        newapp.add("Google pay");
        newapp.add("Paytm");
        
        //Perform addAll() method and removeAll() to the set.
        apps.addAll(newapp);
        System.out.print("addAll() method : ");
        System.out.print(apps+"\n");
        System.out.println();
        System.out.print("removeAll() method : ");
        apps.removeAll(newapp);
        System.out.print(apps+"\n");
        System.out.println();
        newapp.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> app = apps.iterator();
        System.out.println("Print using For loop :");
        while (app.hasNext()) {
            System.out.println(app.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("Print using For each loop :");
        apps.forEach(System.out::println);

        //contains()
        System.out.print("\ncontains() method : ");
        System.out.print("\tChecks app set contains Facebook : " + apps.contains("Facebook"));

        //isEmpty()
        System.out.print("\nisEmpty() method : ");
        System.out.print("\tChecks newapp is empty or not : " + newapp.isEmpty());
	}

}
