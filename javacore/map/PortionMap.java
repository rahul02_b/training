/*
Question :
	4) Write a Java program to get the portion of a map whose keys range from a given key to another key?

1.Requirements
     Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
     GetPortion

3.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.
   
Pseudo Code :
public class PortionMap {

	public static void main(String[] args) {
	
	      TreeMap<Integer, String> car = new TreeMap<Integer, String>();
	      // using put() method to add values to map
	      // using subMap() method get the particular portion of the map
	}

}


*/
package com.kpriet.java.map;

import java.util.TreeMap; 

public class PortionMap {

	public static void main(String[] args) {
	
	      TreeMap<Integer, String> car = new TreeMap<Integer, String>();
	      
	      car.put(32, "BMW");
	      car.put(15, "Tesla");
	      car.put(5, "Honda");
	      car.put(16, "Mahandra");
	      car.put(25, "Skoda");
	      
		  System.out.println("Sub map key-values : " + car.subMap(20, 40));
	}

}
