/*
Question :
	Program to create two paths and test whether they represent same path.	

1.Requirements:
      Program to create two paths and test whether they represent same path.
    
2.Entities:
      CheckSamePath
    
3.Methodsignature:
  	private static void testSameFile(Path path1, Path path2) 
  	
4.Jobs to be done:
    1.Create three Path with specify the path as argument in Path class constructor.
    2.Try block Invoke the testSameFile method.  
       2.1)Check the two file path are same using isSameFile method.
       2.2)Catch Error using printStackTrace method. 
    
Pseudo Code:
	
	public class CheckSamePath {
	  public static void main(String[] args) {
	    Path path1 = Paths.get("C:\1Dev\eclipes\javacore-demo\src\com\kpriet\java\advancenio");
	    Path path2 = Paths.get("C:\1Dev\eclipes\javacore-demo\src\com\kpriet\java\advancenio");
	    Path path3 = Paths.get("C:\1Dev\eclipes\javacore-demo\src\com\kpriet\java\nio");
	
	    testSameFile(path1, path2);
	    testSameFile(path1, path3);
	  }
	  public static void testSameFile(Path path1, Path path2) {
	    try {
	      if (Files.isSameFile(path1, path2)) {
	        System.out.println("Same Path");
	      } else {
	        System.out.println("Not the Same Path");
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
	}

*/

package com.kpriet.java.advancenio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CheckSamePath {
  public static void main(String[] args) {
    Path path1 = Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\advancenio");
    Path path2 = Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\advancenio");
    Path path3 = Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio");

    testSameFile(path1, path2);
    testSameFile(path1, path3);
  }
  private static void testSameFile(Path path1, Path path2) {
    try {
      if (Files.isSameFile(path1, path2)) {
        System.out.println("Same Path");
      } else {
        System.out.println("Not the Same Path");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}