package com.kpriet.java.advancenio;
/*
7. Read a file using java.nio.Files using Paths
--------------------------------WBS---------------------------------------------

1.Requirements:
    - Program to read a file using java.nio.Files using Paths.
    
2.Entities:
    - ReadFileDemo
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Try block
        1.1)Pass file path in get method parameter using Paths class.
        1.2)Read the file by bytes using Files class readAllBytes method.
        1.3)Read the file by lines using Files class readAllLines method.
        1.4)Print the file read bytes and lines.
    2.Catch Exception invoke printStackTrace method.
    
    
Pseudo Code:
''''''''''''

public class ReadFileDemo {

	public static void main(String[] args) {
		
		Path path = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/advance.nio/sample nio.txt");
		try {
			byte[] bs = Files.readAllBytes(path);
			List<String> strings = Files.readAllLines(path);
			
			System.out.println("Read bytes: \n"+new String(bs));
			System.out.println("Read lines: \n"+strings);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
--------------------------------Program Code---------------------------------------
*/
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileDemo {

	public static void main(String[] args) {
		
		Path path = Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\advancenio\\sample nio.txt");
		try {
			byte[] bs = Files.readAllBytes(path);
			List<String> strings = Files.readAllLines(path);
			
			System.out.println("Read bytes: \n"+new String(bs));
			System.out.println("Read lines: \n"+strings);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
