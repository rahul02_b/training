/*
8. Reading a CSV file using java.nio.Files API as List string with each row in CSV as a String
--------------------------------WBS---------------------------------------------

1.Requirements:
    - Program to reading a CSV file using java.nio.Files API as List string with each row in CSV as a String.
    
2.Entities:
    - OpenCsvReader
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Invoke Files class readAllLines to read all lines in file.
         1.1)Get file path using Path class get method in parameter.
    2.Using For each get the string print the CSV file content.
    
    
Pseudo Code:

public class OpenCsvReader {
	public static void main(String... args) throws IOException {
		List<String> lines = Files.readAllLines(Paths
				.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\advancenio\\CSVfile.csv"));
		for (String line : lines) {
			line = line.replace("\"", "");
			System.out.println(line);
		}
	}
}

*/

package com.kpriet.java.advancenio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class OpenCsvReader {
	public static void main(String... args) throws IOException {
		List<String> lines = Files.readAllLines(Paths
				.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\advancenio\\CSVfile.csv"));
		for (String line : lines) {
			line = line.replace("\"", "");
			System.out.println(line);
		}
	}
}


	

