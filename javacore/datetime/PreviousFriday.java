/*
 Question :
 	1. Given a random date, how would you find the date of the previous Friday?

 WBS :
 
 Requirement :
     Given a random date, how would you find the date of the previous Friday?

 Entities :
 	PreviousFriday
 	
 Job to be Done :
 	1. Create class PreviousFriday and main method.
 	2. Create scanner class for getting year, month and date from user.
 	3. Create LocalDate class and LocalDate.of(year,month,date) method for geting year, month and date.
 	4. Print previous friday of the given date using previous() method.
 	
 Pseudo Code :
 public class PreviousFriday {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		// Getting year, month and date
		// Create LocalDate class and LocalDate.of(year,month,date) method for geting year, month and date.
	    // Print previous friday of the given date using previous() method.
	
	}
 }
 

*/

package com.kpriet.java.datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class PreviousFriday {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the year month and date");
		int year = scanner.nextInt();
		int month = scanner.nextInt();
		int date = scanner.nextInt();
		LocalDate thisdate = LocalDate.of(year,month,date);
		System.out.println("The previous Friday is: "+thisdate
				.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
		scanner.close();
	}
}
