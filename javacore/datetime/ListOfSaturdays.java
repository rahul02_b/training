/*
 Question :
 	3. Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

 WBS :
 
 Requirement :
 	Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

 Entities :
 	ListOfSaturdays
 	
 Job to be Done :
 	1. Create class the ListOfSaturdays and main method.
 	2. Create scanner class for getting month name.
 	3. Create month class and converting the month name to upper case.
 	4. Create LocalDate class and get the current year using now() method.
  		4.1 Get month from date 1 find the Saturday.
  	5. Print the list of saturday on the month.
  	
  Pseudo code :
  
  public class ListOfSaturdays {
    public static void main(String[] args) {
    	// create scanner class for getting month name.
    	// Create month class and converting the month name to upper case.
    	// Create LocalDate class and get the current year using now() method.
  		// Get month from date 1 find the Saturday.
  		 while (month2 == month1) {
           //  Print the list of saturday on the month.
        }
    }
  }

*/

package com.kpriet.java.datetime;

import java.time.Month;
import java.time.Year;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListOfSaturdays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the month name to find Saturday : ");
    	String month = scanner.next();
        Month month1 = Month.valueOf(month.toUpperCase());
		
        System.out.println("List of Saturday in : "+ month1);
        LocalDate date = Year.now().atMonth(month1).atDay(1).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month month2 = date.getMonth();
        while (month2 == month1) {
            System.out.print("\n"+ date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            month2 = date.getMonth();
        }
        scanner.close();
    }
}