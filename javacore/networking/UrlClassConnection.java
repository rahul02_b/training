/*
Question :
	5.Create a program  for url class and url connection class in networking.

WBS :

1.Requirements:
     Create a program for url class  in networking

2.Entity:
     UrlClassConnection

3.Jobs to b edone:
    1.Create a class called UrlClass 
    2.Try catch 
       2.1)Create a url path and set the  path name.
       2.2)getProtocol method to get protocol name of website through path name.
       2.3)getHost method to  get hostname .
       2.4)getPort method to get the portnumber.
       2.5)getFile method to get the file name.
    3.Print the results.
    
Pseudo Code:
	
	public class UrlClassConnection {
		public static void main(String[] args) {
			try {
				URL url = new URL("https://www.kpriet.ac.in/");
				System.out.println("Protocol: " + url.getProtocol());
				System.out.println("Host Name: " + url.getHost());
				System.out.println("Port Number: " + url.getPort());
				System.out.println("File Name: " + url.getFile());
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

*/

package com.kpriet.java.networking;

import java.net.URL;

public class UrlClassConnection {

	public static void main(String[] args) {
		try {
			URL url = new URL("https://www.kpriet.ac.in/");

			System.out.println("Protocol: " + url.getProtocol());
			System.out.println("Host Name: " + url.getHost());
			System.out.println("Port Number: " + url.getPort());
			System.out.println("File Name: " + url.getFile());

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
