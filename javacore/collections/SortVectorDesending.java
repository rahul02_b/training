/* 
Question :
	Code for sorting vetor list in descending order.
	
1.Requirements:
     Code for sorting vetor list in descending order.
   
2.Entities
     SortVectorDesending
     
3.Jobs to be done
    1. Create a class SortVectorDesending and main method.
    2. Create a vector name app. 
    3. And add 6 values in the vector
    4. Sorting vetor using sort() method and reverseOrder() in descending order.
    5. Display the result.
    
Pseudo Code :
	public class SortVectorDesending {
		public static void main (String [] args) {
			Vector<String> apps = new Vector<String>();
			// add 6 values in the vector
			// sorting vetor using sort() method and reverseOrder() in descending order
			// print the result
		}
	}

 */
package com.kpriet.java.collections;

import java.util.Collections;
import java.util.Vector;

public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> apps = new Vector<String>();
		apps.add("Youtube");
        apps.add("Amazon");
        apps.add("Flipkart");
        apps.add("Instagram");
        apps.add("Facebook");
        apps.add("MX Player");
        apps.add("Chrome");
        apps.add("Drive");
        apps.add("Gmail");
		System.out.println("Before sorting : "+apps);
		Collections.sort(apps, Collections.reverseOrder());		 
		System.out.println("After sorting : "+apps);
	}
}
