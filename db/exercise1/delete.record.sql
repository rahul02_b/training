-- 7.Delete Records from the table with/without WHERE

-- DELETE RECORD using WHERE

DELETE FROM `database`.`student_details` WHERE (`roll_no` = '10');


-- DELETE RECORD without using WHERE

DELETE FROM `database`.`student` ;
