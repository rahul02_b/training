-- 9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )


-- filter records using AND

SELECT * FROM `database`.student 
WHERE first_name = "praveen" 
AND class >= "10" ;


-- filter records using OR

SELECT * FROM `database`.student 
WHERE roll_no = "101" 
OR class >= "10" ;


-- filter records using NOT

SELECT * FROM `database`.student 
WHERE NOT roll_no >= "102" ;


-- filter records using LIKE

SELECT * FROM `database`.student 
WHERE  class NOT LIKE  "12" ;


-- filter records using IN

SELECT * FROM `database`.student 
WHERE class IN ("3","12","10") ;


-- filter records using ANY

SELECT product_name 
FROM `database`.product 
WHERE product_id = ANY 
( 
SELECT product_id 
FROM `database`.orderdetails 
WHERE quantity > 2
);


-- filter records using WILDCARDS

-- % represents zero or more character

SELECT * FROM `database`.product 
WHERE product_name LIKE "%er%"; 


-- _  represents a single character

SELECT * FROM `database`.product 
WHERE product_name LIKE "char_er"; 

