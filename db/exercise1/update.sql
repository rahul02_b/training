-- 6.Update Field value on the Fields with/without WHERE

-- UPDATE field using WHERE

UPDATE `database`.`student_details` 
SET `name` = 'jegan' 
WHERE (`roll_no` = '17');

-- UPDATE field without using WHERE

UPDATE `database`.`student` 
SET `class` = '101' ;