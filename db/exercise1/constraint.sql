-- 3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, DROP KEY, INDEX


-- NOT NULL

ALTER TABLE `database`.`student_details` 
CHANGE COLUMN `roll_no` `roll_no` INT(11) NOT NULL ,
CHANGE COLUMN `name` `name` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `class` `class` VARCHAR(45) NOT NULL ;


-- DEFAULT

ALTER TABLE `database`.`student_details` 
CHANGE COLUMN `class` `class` VARCHAR(45) NOT NULL DEFAULT '10' ;


-- CHECK

ALTER TABLE `database`.`student`
ADD CHECK (class>=10);


-- PRIMARY KEY

ALTER TABLE `database`.`student_details` 
ADD PRIMARY KEY (`roll_no`);


-- FOREIGN KEY

  ALTER TABLE `database`.`student_details` 
  ADD CONSTRAINT `fk_roll_no`
  FOREIGN KEY (`roll_no`)
  REFERENCES `database`.`student_details` (`roll_no`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;


-- DROP KEY

ALTER TABLE `database`.`student` 
DROP FOREIGN KEY `fk_roll_no`;


-- INDEX 
ALTER TABLE `database`.`student_details` 
ADD INDEX `index_surname` (`roll_no` ASC) ;
;