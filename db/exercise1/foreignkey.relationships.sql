-- 12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

-- CROSS

SELECT *
FROM  `database`.product
CROSS JOIN `database`.orderdetails;
    
-- INNER

SELECT product.product_name 
       , product.product_quantity
       , orderdetails.order_id    
FROM  `database`.product
INNER JOIN `database`.orderdetails   
ON  product.product_id = orderdetails.product_id ;


-- LEFT

SELECT product.product_name 
       , product.product_quantity
       , orderdetails.order_id    
FROM  `database`.product
LEFT JOIN `database`.orderdetails   
ON  product.product_id = orderdetails.product_id ;


-- RIGHT

SELECT product.product_name 
       , product.product_quantity
       , orderdetails.order_id    
FROM  `database`.product
RIGHT JOIN `database`.orderdetails   
ON  orderdetails.order_id = product.product_name ;