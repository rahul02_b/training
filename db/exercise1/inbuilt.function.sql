-- 10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

-- MAX

SELECT MAX(price) AS High_cost 
FROM `database`.product;

-- MIN 

SELECT MIN(price) AS Low_cost 
FROM `database`.product;

-- AVG

SELECT AVG(price) AS average_price 
FROM `database`.product;

-- COUNT

SELECT COUNT(price) AS total_no_of_products 
FROM `database`.product;

--  FIRST

SELECT * FROM `database`.product 
LIMIT 1;

-- LAST

SELECT * FROM `database`.product 
ORDER BY product_id 
DESC LIMIT 1;

-- SUM

SELECT SUM(price) AS total_cost 
FROM `database`.product;