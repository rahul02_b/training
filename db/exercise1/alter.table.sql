-- 2.Alter Table with Add new Column and Modify\Rename\Drop column


-- ADD NEW COLUMN

ALTER TABLE `database`.`student_details` 
ADD COLUMN `name` VARCHAR(45) NULL AFTER `id`,
ADD COLUMN `class` VARCHAR(45) NULL AFTER `name`;


-- Rename column

ALTER TABLE `database`.`student_details` 
CHANGE COLUMN `id` `roll_no` INT(11) NULL DEFAULT NULL ;


-- DROP COLUMN

ALTER TABLE `database`.`student_details` 
DROP COLUMN `class`;
