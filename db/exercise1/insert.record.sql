-- 5.Insert Records into Table

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('10', 'Deepak', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('11', 'Deepika', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('12', 'Ezhil', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('13', 'Feroz', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('14', 'Gautham', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('15', 'Harish', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('16', 'Harini', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('17', 'jagadeesh', '11');

INSERT INTO `database`.`student_details` (`roll_no`, `name`, `class`) 
VALUES ('18', 'Kavin', '11');