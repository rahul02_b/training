-- 4. List Employees details from a particular university along with their 
-- college and department details. Details should be sorted by rank and college name 

SELECT employee.id AS 'employee_id'
      ,employee.name AS 'employee_name'
      ,employee.dob
      ,employee.email
      ,employee.phone
      ,designation.name
      ,designation.rank
      ,college.name AS 'college_name'
      ,university.univ_code
      ,university.university_name
FROM employee
    ,college
    ,university
    ,designation
WHERE college.id = employee.college_id
 AND college.univ_code = university.univ_code
 AND designation.id = employee.desig_id
 AND university.university_name = 'amritha deemed university'
ORDER BY college.name,designation.rank
