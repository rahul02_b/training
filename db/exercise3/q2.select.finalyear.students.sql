-- 2. Select final year students(Assume that universities has Engineering 
-- Depts only) details who are studying under a particular university and selected cities alone. 
        -- ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS, 
        -- COLLEGE_NAME, DEPARTMENT_NAME 

SELECT student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name` AS 'college_name'
      ,department.`dept_name` AS 'department_name'
FROM department
     ,student 
     ,college_department
     ,college
     ,university
WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'amritha deemed university'
   AND college.city = 'Coimbatore'
   AND student.academic_year = '2020'
