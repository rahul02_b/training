-- 5. List Students details along with their GRADE,CREDIT and GPA details 
-- from all universities. Result should be sorted by college_name and semester. Apply paging also. 

SELECT student.`id` AS 'student_id'
      ,student.`roll_number`
      ,student.`name` AS 'student_name'
      ,student.`dob`
      ,student.`gender`
      ,student.`phone` 
      ,college.`name` AS 'college_ame'
      ,semester_result.`semester`
      ,semester_result.`grade`
      ,semester_result.`credits`
 FROM student
     ,university
     ,semester_result
     ,college
WHERE university.`univ_code` = college.`univ_code`
 AND student.`id` = semester_result.`stud_id`
 AND college.`id` = student.`college_id`
ORDER BY college.`name`, semester_result.`semester`