/* 8. Find employee vacancy position in all the departments from all the 
colleges and across the universities. Result should be populated with 
following information.. Designation, rank, college, department and university details.  */

ALTER TABLE `university`.`employee` 
CHANGE COLUMN `name` `name` VARCHAR(100) NULL ,
CHANGE COLUMN `dob` `dob` DATE NULL ,
CHANGE COLUMN `email` `email` VARCHAR(50) NULL ,
CHANGE COLUMN `phone` `phone` BIGINT NULL ;

INSERT INTO `university`.`employee` (`id`
                                               ,`college_id`
                                               ,`cdept_id`
                                               ,`desig_id`) 
VALUES ('05'
       ,'5'
       ,'5'
       ,'3');
INSERT INTO `university`.`employee` (`id`
                                               ,`college_id`
                                               ,`cdept_id`
                                               ,`desig_id`) 
VALUES ('06'
       ,'6'
       ,'6'
       ,'3');

SELECT designation.name AS 'designation'
      ,designation.rank
      ,college.name AS 'college_name'
      ,department.dept_name
      ,university.university_name
FROM employee
    ,college_department
    ,department
    ,university 
    ,designation 
    ,college
WHERE employee.name is NULL
 AND college.univ_code = university.univ_code 
 AND department.univ_code = university.univ_code
 AND college_department.college_id = college.id 
 AND college_department.udept_code = department.dept_code
 AND employee.college_id = college.id
 AND employee.cdept_id = college_department.cdept_id 
 AND employee.desig_id = designation.id
