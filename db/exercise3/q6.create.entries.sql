/* 6. Create new entries in SEMESTER_FEE table for each student from all 
the colleges and across all the universities. These entries should be 
created whenever new semester starts.. Each entry should have below 
default values; 
     a) AMOUNT - Semester fees 
     b) PAID_YEAR - Null 
     c) PAID_STATUS - Unpaid  */

ALTER TABLE `university management`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE(18,2) NULL DEFAULT 85000 ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NOT NULL DEFAULT 'un_paid' ;

INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`)
VALUES ('06'
       ,'11'
       ,'15'
       ,'26');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('07'
       ,'12'
       ,'17'
       ,'27');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('08'
       ,'16'
       ,'18'
       ,'28');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('09'
       ,'04'
       ,'19'
       ,'29');
INSERT INTO `university management`.`semester_fee` (`bill_no`
                                                   ,`cdept_id`
                                                   ,`stud_id`
                                                   ,`semester`) 
VALUES ('10'
       ,'05'
       ,'20'
       ,'30');
