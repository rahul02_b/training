-- 2. Write a query to insert at least 5 employees for each department


INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`)
VALUES ('1', 'Rahul', 'B', '2001/02/06', '2018/06/09', '1300000', '1');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('2', 'pragadheesh', 'ps', '1999/07/12', '2015/01/04', '1400000', '1');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('3', 'deepak', 'l', '1998/03/12', '2019/02/10', '1240000', '1');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('4', 'Ezhil', 'a', '2000/09/09', '2020/07/12', '1500000', '1');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('5', 'Feroz', 'b', '2002/12/06', '2017/06/03', '200000', '2');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('6', 'Gautham', 'c', '1994/05/09', '2018/06/09', '625000', '2');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('7', 'Harish', 'd', '1987/04/12', '2015/04/02', '645000', '2');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('8', 'Indhu', 'e', '1999/06/08', '2019/10/02', '240000', '2');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('9', 'Jayasree', 'f', '1999/02/12', '2019/06/04', '150000', '3');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('10', 'Kavin', 'g', '2000/03/09', '2020/06/12', '850000', '3');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('11', 'Deepika', 'h', '1998/01/07', '2017/01/02', '540000', '3');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('12', 'logesh', 'i', '1997/09/01', '2019/05/12', '225000', '3');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('13', 'manoj', 'j', '1998/09/12', '2015/12/01', '757000', '4');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('14', 'mervin', 'k', '1998/01/12', '2012/12/12', '325000', '4');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('15', 'naveen', 'l', '1989/02/12', '2011/11/11', '650000', '4');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('16', 'balaji', 'm', '2002/12/09', '2010/10/10', '750000', '4');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('17', 'praveen', 'm', '2000/02/12', '2009/09/09', '845000', '5');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('18', 'ramesh', 'n', '1991/06/12', '2018/08/08', '340000', '5');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('19', 'mahesh', 'r', '2000/12/12', '2017/07/12', '1500500', '5');

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('20', 'suresh', 'e', '1999/06/12', '2017/08/12', '1540000', '5');
