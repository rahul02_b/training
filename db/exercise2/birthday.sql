-- 9. Write a query to find out employee birthday falls on that day

SELECT first_name,dob
FROM `database`.employee
WHERE MONTH(dob) = MONTH(curdate());