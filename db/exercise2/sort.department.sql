--  11. Write a query to get employee names and their respective department name

SELECT employee.first_name,employee.surname,department.department_name  
  FROM  database.employee empl , database.department dept  
 WHERE  empl.department_number = dept.department_number;
