-- 5. Find out the highest and least paid in all the department

-- highest paid in all department

SELECT 
  department_number
, MAX(annual_salary) 
AS highest_salary 
FROM `database`.employee 
GROUP BY department_number;