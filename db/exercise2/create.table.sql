
-- 1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility

--    employee number must be the primary key in employee table
--    department number must be the primary key in department table
--    department number is the foreign key in the employee table

-- Create table for Employee


CREATE TABLE `database`.`employee` 
(
  `emp_id` INT NOT NULL
  ,`first_name` VARCHAR(45) NOT NULL
  ,`surname` VARCHAR(45) NOT NULL
  ,`dob` DATE NOT NULL
  ,`date_of_joining` DATE NOT NULL
  ,`annual_salary` INT NOT NULL
);

ALTER TABLE `database`.`employee` 
ADD COLUMN `department_number` INT NOT NULL 
AFTER `annual_salary`;


--    employee number must be the primary key in employee table


ALTER TABLE `database`.`employee` 
ADD PRIMARY KEY (`emp_id`);


-- Create table for Department 


CREATE TABLE `database`.`department`
 (
  `department_number` INT NOT NULL
  );

ALTER TABLE `database`.`department` 
ADD COLUMN `department_name` VARCHAR(45) NOT NULL 
AFTER `department_number`;


--    department number must be the primary key in department table

ALTER TABLE `database`.`department` 
ADD PRIMARY KEY (`department_number`);


INSERT INTO `database`.`department` (`department_number`, `department_name`) 
VALUES ('1', 'ITDesk');
INSERT INTO `database`.`department` (`department_number`, `department_name`) 
VALUES ('2', 'Finance');
INSERT INTO `database`.`department` (`department_number`, `department_name`) 
VALUES ('3', 'Engineering');
INSERT INTO `database`.`department` (`department_number`, `department_name`) 
VALUES ('4', 'HR');
INSERT INTO `database`.`department` (`department_number`, `department_name`) 
VALUES ('5', 'Facility');


--   department number is the foreign key in the employee table

ALTER TABLE `database`.`employee` 
ADD CONSTRAINT `fk_dept_num`
FOREIGN KEY (`department_number`)
REFERENCES `database`.`department` (`department_number`)
ON DELETE RESTRICT
ON UPDATE CASCADE;


