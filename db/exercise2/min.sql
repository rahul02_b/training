-- 5. Find out the highest and least paid in all the department

-- least paid in all department

SELECT 
  department_number
, MIN(annual_salary) 
AS least_salary 
FROM `database`.employee 
GROUP BY department_number;