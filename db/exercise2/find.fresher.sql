-- 10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.

SELECT first_name
FROM `database`.employee
WHERE department_number IS NULL ;