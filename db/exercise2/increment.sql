-- 4. Write a query to increment 10% of every employee salary
 
 UPDATE employee 
 SET annual_salary = annual_salary + (annual_salary * 0.1);