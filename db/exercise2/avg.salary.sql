-- 6. Find out the average salary


SELECT department_number
  , AVG(annual_salary) 
AS average_salary 
FROM `database`.employee ;